"""Greet function"""


def greet(name, location):
    """Greet with name and location"""
    print(f"Hello {name}!")
    print(f"What is it like in {location}?")


greet("Jorge", "Mexico")
greet(location="Japan", name="Lopez")
