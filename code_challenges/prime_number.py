"""Prime number checker"""


def prime_checker(number):
    """Prime checker function"""

    # For positive integers only
    if number == 1:
        finished = True
        result = " not "
    else:
        finished = False
        result = " "
        i = 2
    while not finished:
        if i == number:
            finished = True
        elif (number % i) == 0:
            result = " not "
            finished = True
        i += 1
    print(f"it's{result}a prime number.")


n = int(input("Check this number: "))
prime_checker(number=n)
