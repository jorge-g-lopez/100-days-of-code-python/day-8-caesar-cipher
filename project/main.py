"""Caesar Cipher"""

alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
]


def caesar(direction, message, shift):
    """Caesar cipher function"""
    new_message = ""
    if direction == "encode":
        len_alphabet = len(alphabet)
        for letter in message:
            if letter in alphabet:
                new_message += alphabet[(alphabet.index(letter) + shift) % len_alphabet]
            else:
                new_message += letter
    else:
        final_shift = shift % len(alphabet)
        for letter in message:
            if letter in alphabet:
                new_message += alphabet[alphabet.index(letter) - final_shift]
            else:
                new_message += letter

    print(f"The {direction}d text is {new_message}")


GO_AGAIN = "Type 'yes' if you want to go again. Otherwise type 'no': "
go = "yes"
while go == "yes":
    dirc = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text = input("Type your message:\n").lower()
    shft = int(input("Type the shift number:\n"))
    caesar(dirc, text, shft)
    go = input(GO_AGAIN).lower()
print("Goodbye")
